This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "event-publisher-library"

## [v1.2.0]
- Restored correct behavior for event publishing with workflow id only sent back, setting input payload in JSON
- Extracted `AbstractHTTPWithJWTTokenAuthEventSender` class for easy subclassing
- Added new outcome check methods to inspect last send and last check actions results and some other facilities
- Better handling of exceptions and retrying behavior in case of read timeout. (Default connection timeout is 10s and read timeout now is 60s)
- Moved from `maven-portal-bom` to `gcube-bom`

## [v1.1.0]
- Added `BufferedEventProcessor` to manual send bunch of events and controlling their output status (#23628)

## [v1.0.2]
- Refactored/extracted event sender that uses OIDC token only and extended it for the UMA version.

## [v1.0.1]
- Tuned some log levels and content sending the event and removed the use of deprecated code.

## [v1.0.0-SNAPSHOT]
- First release (#19461)
