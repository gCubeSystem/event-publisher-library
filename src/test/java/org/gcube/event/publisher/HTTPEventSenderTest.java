package org.gcube.event.publisher;

import java.net.MalformedURLException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HTTPEventSenderTest {

    protected static final Logger logger = LoggerFactory.getLogger(HTTPEventSenderTest.class);

    public HTTPEventSenderTest() {
    }

    public static void main(String[] args) {
        EventPublisher publisher = new AbstractEventPublisher() {
            @Override
            protected EventSender createEventSender() {
                try {
                    return new HTTPWithOIDCAuthEventSender(
                            new URL("https://conductor.cloud-dev.d4science.org/api/workflow/"),
                            "lr62_portal",
                            "6937125d-6d70-404a-9d63-908c1e6415c4",
                            new URL("https://accounts.dev.d4science.org/auth/realms/d4science/protocol/openid-connect/token"));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        String id = publisher.publish(new Event("test", "test", "test"), true);
        logger.info("*** Published: {}", id);
        logger.info("*** Publish was {} and HTTP response status is: {}",
                publisher.isLastPublishOK() ? "OK" : "KO", publisher.getLastPublishEventHTTPResponseCode());

        EventStatus status = null;
        do {
            status = publisher.check(id);
            logger.trace("*** Check was {} and HTTP response status is: {}", publisher.isLastCheckOK() ? "OK" : "KO",
                    publisher.getLastCheckHTTPResponseCode());
        } while (status.getStatus() != EventStatus.Status.COMPLETED && status.getStatus() != EventStatus.Status.FAILED);

        logger.info("*** Final status is: {}", status.toString());

        status = publisher.publishAndCheck(new Event("test", "test", "test"), 1000);
        logger.info("*** Publish was {} and HTTP response status is: {}",
                publisher.isLastPublishOK() ? "OK" : "KO", publisher.getLastPublishEventHTTPResponseCode());

        do {
            status = publisher.refresh(status);
            logger.trace("*** Check was {} and HTTP response status is: {}", publisher.isLastCheckOK() ? "OK" : "KO",
                    publisher.getLastCheckHTTPResponseCode());
        } while (status.getStatus() != EventStatus.Status.COMPLETED && status.getStatus() != EventStatus.Status.FAILED);

        logger.info("*** Final status is: {}", status.toString());
    }

}
