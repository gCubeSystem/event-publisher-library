package org.gcube.event.publisher;

import java.net.HttpURLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractEventPublisher implements EventPublisher {

    protected static final Logger logger = LoggerFactory.getLogger(AbstractEventPublisher.class);

    private EventSender eventSender;
    private ResultsParser resultsParser;

    public AbstractEventPublisher() {
        this.eventSender = createEventSender();
        this.resultsParser = createResultsParser();
    }

    @Override
    public void publish(Event event) {
        publish(event, false);
    }

    @Override
    public String publish(Event event, boolean waitForResult) {
        if (event != null) {
            if (waitForResult) {
                return getEventSender().sendAndGetResult(event);
            } else {
                getEventSender().send(event);
            }
        } else {
            logger.warn("Cannot publish a null event");
        }
        return null;
    }

    @Override
    public boolean isLastPublishOK() {
        return getLastPublishEventHTTPResponseCode() == HttpURLConnection.HTTP_OK
                || getLastPublishEventHTTPResponseCode() == HttpURLConnection.HTTP_CREATED;
    }

    @Override
    public int getLastPublishEventHTTPResponseCode() {
        return getEventSender().getLastSendHTTPResponseCode();
    }

    @Override
    public EventStatus publishAndCheck(Event event) {
        return publishAndCheck(event, 0);
    }

    @Override
    public EventStatus publishAndCheck(Event event, int delayMS) {
        String instanceId = publish(event, true);
        try {
            if (delayMS > 0) {
                Thread.sleep(delayMS);
            }
            return check(instanceId);
        } catch (InterruptedException e) {
            logger.error("Sleeping before performing the event status check", e);
            return null;
        }
    }

    @Override
    public EventStatus check(String instanceId) {
        if (instanceId != null) {
            return getResultsParser().parseResults(instanceId, eventSender.retrive(instanceId));
        } else {
            logger.warn("Cannot check with a null instance ID");
            return EventStatus.NOT_FOUND(instanceId);
        }
    }

    @Override
    public EventStatus refresh(EventStatus eventStatus) {
        return check(eventStatus.getInstanceId());
    }

    @Override
    public boolean isLastCheckOK() {
        return getLastCheckHTTPResponseCode() == 200;
    }

    @Override
    public int getLastCheckHTTPResponseCode() {
        return getEventSender().getLastRetrieveHTTPResponseCode();
    }

    protected abstract EventSender createEventSender();

    protected ResultsParser createResultsParser() {
        return new ConductorResultsParser();
    }

    public EventSender getEventSender() {
        return eventSender;
    }

    public void setEventSender(EventSender eventSender) {
        this.eventSender = eventSender;
    }

    public ResultsParser getResultsParser() {
        return resultsParser;
    }

    public void setResultsParser(ResultsParser resultsParser) {
        this.resultsParser = resultsParser;
    }

}
