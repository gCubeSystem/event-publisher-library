package org.gcube.event.publisher;

public interface EventPublisher {

    /**
     * Publish a new event and nothing more. The sender is not interested to the success/failure of the send.
     * The results on the workflow engine, is the start of an instance of the workflow identified by the {@link Event#getName()} string.
     * @param event the event to be published
     */
    void publish(Event event);

    /**
     * Publishes a new event and optionally wait for the result.
     * If <code>waitForResult</code> parameter is <code>false</code> the behavior is the same of the {@link #publish(Event)} method,
     * if true, the workflow id is returned as string if the publish had success.
     * @param event the vent to be published
     * @param waitForResult if the sender is interested or not to the resulting workflow id
     * @return the resulting workflow id
     */
    String publish(Event event, boolean waitForResult);

    /**
     * Tells if the last publish was a success or not.
     * @return <code>true</code> if the publish was OK, <code>false</code> otherwise
     */
    boolean isLastPublishOK();

    /**
     * Returns the last returned HTTP response code of a publish. E.g. 200 if the send was OK or 404 if the event doesn't have a corresponding workflow definition.
     * @return the HTTP response code of the last publish or -1 if an error occurred before the call (e.g. during the authorization or connection)
     */
    int getLastPublishEventHTTPResponseCode();

    /**
     * Publish an event and immediately checks for the results status.
     * The behavior is the same of the {@link #publishAndCheck(Event, int)} with <code>delayMS</code> argument less or equal to 0.
     * @param event the event to be published
     * @return an object with info about the event's running status
     */
    EventStatus publishAndCheck(Event event);

    /**
     * Publish an event and checks for the results status after a delay.
     * The behavior is the same of the {@link #publishAndCheck(Event)} if delayMS argument is less or equal to 0.
     * @param event the event to be published
     * @param delayMS the delay betwen the publish and the query calls
     * @return an object with info about the event's running status
     */
    EventStatus publishAndCheck(Event event, int delayMS);

    /**
     * Checks for the workflow results status.
     * @param instanceId the workflow instance id, resulting of the {@link #publish(Event, boolean)} with <code>waitForResult</code> as true.
     * @return an object with info about the event's running status
     */
    EventStatus check(String instanceId);

    /**
     * Refreshes an event status by checking for the  status of the workflow execution represented by the {@link EventStatus#getInstanceId()} string.
     * @param eventStatus a previously obtained event status.
     * @return an object with new info about the event's running status
     */
    EventStatus refresh(EventStatus eventStatus);

    /**
     * Tells if the last check was a success or not.
     * @return <code>true</code> if the publish was OK, <code>false</code> otherwise
     */
    boolean isLastCheckOK();

    /**
     * Returns the last returned HTTP response code of a check. E.g. 200 if the send was OK or 404 if the event doesn't have a corresponding workflow instance.
     * @return the HTTP response code of the last publish or -1 if an error occurred before the call (e.g. during the authorization or connection)
     */
    int getLastCheckHTTPResponseCode();

}