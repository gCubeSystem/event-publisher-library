package org.gcube.event.publisher;

import org.json.simple.JSONObject;

public class StartWorkflow extends JSONObject {

    private static final long serialVersionUID = -6974427433855594349L;

    // Name of the Workflow. MUST be registered with Conductor before starting workflow
    public static final String NAME_ENTRY = "name";

    // Workflow version    defaults to latest available version
    private static final String VERSION_ENTRY = "version";

    // JSON object with key value params, that can be used by downstream tasks See Wiring Inputs and Outputs for details
    private static final String INPUT_ENTRY = "input";

    // Unique Id that correlates multiple Workflow executions  optional
    private static final String CORRELATION_ID_ENTRY = "correlationId";

    // See Task Domains for more information.  optional
    private static final String TASK_TO_DOMAIN_ENTRY = "taskToDomain";

    // An adhoc Workflow Definition to run, without registering. See Dynamic Workflows.    optional
    private static final String WORKFLOW_DEF_ENTRY = "workflowDef";

    // This is taken care of by Java client. See External Payload Storage for more info.   optional
    private static final String EXTERNAL_INPUT_PAYLOAD_STORAGE_PATH_ENTRY = "externalInputPayloadStoragePath";

    // Priority level for the tasks within this workflow execution. Possible values are between 0 - 99
    private static final String PRIORITY_ENTRY = "priority";

    public StartWorkflow(Event event) {
        this(event.getName(), event);
    }

    public StartWorkflow(String name, JSONObject input) {
        super();
        setName(name);
        setInput(input);
    }

    @SuppressWarnings("unchecked")
    protected void set(String key, Object value) {
        put(key, value);
    }

    public void setName(String name) {
        set(NAME_ENTRY, name);
    }

    public void setVersion(Integer version) {
        set(VERSION_ENTRY, version);
    }

    public void setInput(JSONObject input) {
        set(INPUT_ENTRY, input);
    }

    public void setCorrelationId(String correlationId) {
        set(CORRELATION_ID_ENTRY, correlationId);
    }

    public void setTaskToDomain(String taskToDomain) {
        set(TASK_TO_DOMAIN_ENTRY, taskToDomain);
    }

    public void setWorkflowDef(JSONObject workflowDef) {
        set(WORKFLOW_DEF_ENTRY, workflowDef);
    }

    public void setExternalInputPayloadStoragePath(String externalInputPayloadStoragePath) {
        set(EXTERNAL_INPUT_PAYLOAD_STORAGE_PATH_ENTRY, externalInputPayloadStoragePath);
    }

    public void setPriority(Integer prioriry) {
        set(PRIORITY_ENTRY, prioriry);
    }

}