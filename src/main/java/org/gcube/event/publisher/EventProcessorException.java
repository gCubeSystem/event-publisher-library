package org.gcube.event.publisher;

public class EventProcessorException extends Exception {

    private static final long serialVersionUID = 568668418901859597L;

    public EventProcessorException(String message) {
        super(message);
    }

    public EventProcessorException(String message, Throwable cause) {
        super(message, cause);
    }
}
