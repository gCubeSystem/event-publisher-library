package org.gcube.event.publisher;

import org.json.simple.JSONObject;

public class EventStatus {

    public enum Status {
        RUNNING, COMPLETED, FAILED, TIMED_OUT, TERMINATED, PAUSED, NOT_FOUND;
    }

    public static EventStatus RUNNING(String instanceId, JSONObject input) {
        return new EventStatus(instanceId, Status.RUNNING, input);
    }

    public static EventStatus COMPLETED(String instanceId, JSONObject input, JSONObject output) {
        return new EventStatus(instanceId, Status.COMPLETED, input, output);
    }

    public static EventStatus FAILED(String instanceId, JSONObject input, JSONObject output) {
        return new EventStatus(instanceId, Status.FAILED, input, output);
    }

    public static EventStatus TIMED_OUT(String instanceId, JSONObject input) {
        return new EventStatus(instanceId, Status.TIMED_OUT, input);
    }

    public static EventStatus TERMINATED(String instanceId, JSONObject input, JSONObject output) {
        return new EventStatus(instanceId, Status.TERMINATED, input, output);
    }

    public static EventStatus PAUSED(String instanceId, JSONObject input) {
        return new EventStatus(instanceId, Status.PAUSED, input);
    }

    public static EventStatus NOT_FOUND(String instanceId) {
        return new EventStatus(instanceId, Status.NOT_FOUND);
    }

    private String instanceId;
    private Status status;
    private JSONObject input;
    private JSONObject output;

    private EventStatus(String uuid, Status status) {
        this(uuid, status, null);
    }

    private EventStatus(String instanceId, Status status, JSONObject input) {
        this(instanceId, status, input, null);
    }

    private EventStatus(String instanceId, Status status, JSONObject input, JSONObject output) {
        setInstanceId(instanceId);
        setStatus(status);
        setInput(input);
        setOutput(output);
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    public void setInput(JSONObject input) {
        this.input = input;
    }

    public JSONObject getInput() {
        return input;
    }

    public void setOutput(JSONObject output) {
        this.output = output;
    }

    public JSONObject getOutput() {
        return output;
    }

    @Override
    public String toString() {
        return String.format("[%s]\ninput: %s\noutput: %s", status, input != null ? input.toJSONString() : "<No input>",
                output != null ? output.toJSONString() : "<No output>");
    }

}