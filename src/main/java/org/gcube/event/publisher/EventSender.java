package org.gcube.event.publisher;

import org.json.simple.JSONObject;

public interface EventSender {

    /**
     * Sends an event.
     * @param event the event to send
     */
    void send(Event event);

    /**
     * Return the last send HTTP response code.
     * @return the HTTP response code or -1 if an error occurred before the call (e.g. during the authorization or connection)
     */
    int getLastSendHTTPResponseCode();

    /**
     * Send an event and get results.
     * @param event the event to send
     * @return the result of the call
     */
    String sendAndGetResult(Event event);

    /**
     * Retrieves the instance of the provided workflow instance.
     * @param id the workflow instance id
     * @return
     */
    JSONObject retrive(String id);

    /**
     * Return the last retrieve HTTP response code.
     * @return the HTTP response code or -1 if an error occurred before the call (e.g. during the authorization or connection)
     */
    int getLastRetrieveHTTPResponseCode();

}