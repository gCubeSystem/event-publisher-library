package org.gcube.event.publisher;

import java.net.URL;

import org.gcube.oidc.rest.JWTToken;
import org.gcube.oidc.rest.OpenIdConnectRESTHelper;
import org.gcube.oidc.rest.OpenIdConnectRESTHelperException;

public class HTTPWithOIDCAuthEventSender extends AbstractHTTPWithJWTTokenAuthEventSender implements EventSender {

    public HTTPWithOIDCAuthEventSender(URL baseEndpointURL, String clientId, String clientSecret, URL tokenURL) {
        super(baseEndpointURL, clientId, clientSecret, tokenURL);
    }

    protected JWTToken getAuthorizationToken() throws OpenIdConnectRESTHelperException {
        if (clientId != null && clientSecret != null && tokenURL != null) {
            log.debug("Getting OIDC token for clientId '{}' from: {}", clientId, tokenURL);
            return OpenIdConnectRESTHelper.queryClientToken(clientId, clientSecret, tokenURL);
        } else {
            log.debug("Can't get OIDC token since not all the required params were provied");
            return null;
        }
    }

}