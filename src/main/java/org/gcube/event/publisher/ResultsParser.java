package org.gcube.event.publisher;

import org.json.simple.JSONObject;

public interface ResultsParser {

    EventStatus parseResults(String uuid, JSONObject results);

}
