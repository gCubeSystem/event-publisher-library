package org.gcube.event.publisher;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConductorResultsParser implements ResultsParser {

    protected static final Logger logger = LoggerFactory.getLogger(ConductorResultsParser.class);

    public ConductorResultsParser() {
    }

    @Override
    public EventStatus parseResults(String uuid, JSONObject results) {
        EventStatus eventStatus = null;
        if (results != null) {
            JSONObject input = (JSONObject) results.get("input");
            JSONObject output = (JSONObject) results.get("output");
            switch ((String) results.get("status")) {
            case "COMPLETED":
                eventStatus = EventStatus.COMPLETED(uuid, input, output);
                break;
            case "FAILED":
                eventStatus = EventStatus.FAILED(uuid, input, output);
                break;
            case "NOT_FOUND":
                eventStatus = EventStatus.NOT_FOUND(uuid);
                break;
            case "PAUSED":
                eventStatus = EventStatus.PAUSED(uuid, input);
                break;
            case "RUNNING":
                eventStatus = EventStatus.RUNNING(uuid, input);
                break;
            case "TERMINATED":
                eventStatus = EventStatus.TERMINATED(uuid, input, output);
                break;
            default:
                eventStatus = EventStatus.NOT_FOUND(uuid);
            }
        } else {
            logger.warn("Nothing to parse since JSON object is null");
        }
        return eventStatus;
    }

}
