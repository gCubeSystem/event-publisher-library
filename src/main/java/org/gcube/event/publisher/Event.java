package org.gcube.event.publisher;

import java.time.OffsetDateTime;
import java.util.Map;
import java.util.UUID;

import org.json.simple.JSONObject;

public class Event extends JSONObject {

    private static final long serialVersionUID = 1081109731437677614L;

    public static final String UUID_ENTRY = "uuid";
    public static final String TIMESTAMP_ENTRY = "timestamp";
    public static final String NAME_ENTRY = "name";
    public static final String TYPE_ENTRY = "type";
    public static final String SENDER_ENTRY = "sender";

    public Event(String name, String type, String sender) {
        this(name, type, sender, null);
    }

    public Event(String name, String type, String sender, Map<String, String> data) {
        setUUID(UUID.randomUUID());
        setTimestamp(OffsetDateTime.now());
        setName(name);
        setType(type);
        setSender(sender);
        if (data != null) {
            setAll(data);
        }
    }

    @SuppressWarnings("unchecked")
    protected void set(String key, String value) {
        put(key, value);
    }

    @SuppressWarnings("unchecked")
    protected void setAll(Map<String, String> entries) {
        putAll(entries);
    }

    public void setUUID(UUID uuid) {
        set(UUID_ENTRY, uuid.toString());
    }

    public UUID getUUID() {
        return UUID.fromString((String) get(UUID_ENTRY));
    }

    public void setTimestamp(OffsetDateTime dateTime) {
        set(TIMESTAMP_ENTRY, dateTime.toString());
    }

    public OffsetDateTime getTimestamp() {
        return OffsetDateTime.parse((CharSequence) get(TIMESTAMP_ENTRY));
    }

    public void setName(String name) {
        set(NAME_ENTRY, name);
    }

    public String getName() {
        return (String) get(NAME_ENTRY);
    }

    public void setType(String type) {
        set(TYPE_ENTRY, type);
    }

    public String getType() {
        return (String) get(TYPE_ENTRY);
    }

    public void setSender(String sender) {
        set(SENDER_ENTRY, sender);
    }

    public String getSender() {
        return (String) get(SENDER_ENTRY);
    }

}