package org.gcube.event.publisher;

import java.net.URL;

import org.gcube.oidc.rest.JWTToken;
import org.gcube.oidc.rest.OpenIdConnectRESTHelper;
import org.gcube.oidc.rest.OpenIdConnectRESTHelperException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HTTPWithUMAAuthEventSender extends HTTPWithOIDCAuthEventSender {

    protected static final Logger log = LoggerFactory.getLogger(HTTPWithUMAAuthEventSender.class);

    private String umaAudience;

    public HTTPWithUMAAuthEventSender(URL baseEndpointURL, String clientId, String clientSecret, URL tokenURL,
            String umaAudience) {

        super(baseEndpointURL, clientId, clientSecret, tokenURL);
        this.umaAudience = umaAudience;
    }

    @Override
    protected JWTToken getAuthorizationToken() throws OpenIdConnectRESTHelperException {
        JWTToken oidcToken = super.getAuthorizationToken();
        if (oidcToken != null) {
            if (umaAudience != null) {
                log.debug("Getting UMA token with audience '{}' from: {}", umaAudience, getTokenURL());
                return OpenIdConnectRESTHelper.queryUMAToken(getTokenURL(), oidcToken.getAccessTokenAsBearer(),
                        umaAudience, null);
            } else {
                log.debug("Can't get UMA token since the required param was not provied");
            }
        }
        return oidcToken;
    }

}